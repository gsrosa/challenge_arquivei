# Front-end Challenge Arquivei

This is challenge for a job position.

### Brief description
This application is a part of a flux used in pre account project.

## Getting Started

Clone this repo:

```
$ git clone https://gsrosa@bitbucket.org/gsrosa/challenge_arquivei.git
$ cd challenge_arquivei
```

## Installing

Assuming you have all the Node.js and Yarn environment setup in your machine:

To install your SERVER application dependencies:
```
$ cd server
$ yarn install
```
To install your CLIENT application dependencies:
```
$ cd client
$ yarn install
```

After install all dependencies, to start your SERVER application go to Server directory and run:

```
$ yarn start
```

And to start your CLIENT application also go to Client directory and run:
```
$ yarn start
```

## Running CLIENT tests

To run the CLIENT application tests, go to client directory and run:

```
$ yarn test
```

## Built With

* [EcmaScript6](http://es6-features.org/#Constants) - The programming language used for the Client
* [React.js](https://reactjs.org/) - The UI library used for the Client
* [Node.js](http://nodejs.org) - The programming language used for the Server
* [Express](https://expressjs.com/pt-br/) - The web framework used
* [Yarn](https://yarnpkg.com/en/) - Dependency Management
* [Sass](https://sass-lang.com/guide) - CSS Preprocessor
* [Webpack](https://webpack.js.org) - Module Bundler
* [Babel](https://babeljs.io/) - JavaScript Compiler

## Author

* **Gabriel Rosa**