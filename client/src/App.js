import React from "react"
import ReactDOM from "react-dom"
import Header from "./components/header"
import CompanyInformation from "./components/company-information"
import SubHeader from "./components/sub-header"
import Table from "./components/table"
import ContainerBox from "./components/container-box"

const App = () => {
  return (
    <div>
      <Header />
      <CompanyInformation />
      <hr />
      <SubHeader />
      <Table />
      <ContainerBox />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById("root"))
