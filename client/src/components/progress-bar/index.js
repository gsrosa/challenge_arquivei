import React from 'react';

const ProgressBar = () => (
  <div>
    <div className="progress">
      <div className="loading" />
    </div>
  </div>
);

export default ProgressBar;
