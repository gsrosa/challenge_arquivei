import React from 'react';
import { shallow } from 'enzyme';
import ProgressBar from './index';

import EnzymeConfig from '../../enzyme-config';

describe('ProgressBar Component', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<ProgressBar />);
    expect(wrapper).toMatchSnapshot();
  });
});
