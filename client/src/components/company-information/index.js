import React, { Component } from 'react';
import axios from 'axios';
import { format as formatCnpj } from '@fnando/cnpj';

export const uri = 'http://0.0.0.0:8081';

class CompanyInformation extends Component {
  constructor() {
    super();
    this.state = {
      province: '',
      cnpj: '',
      companyName: ''
    };
  }

  componentWillMount() {
    axios.get(`${uri}/initial-state`).then(res => {
      this.setState({
        province: res.data.networkGrowth.currentCompany.state,
        cnpj: formatCnpj(res.data.networkGrowth.currentCompany.cnpj),
        companyName: res.data.networkGrowth.currentCompany.name
      });
    });
  }

  render() {
    return (
      <div className="company-information">
        <p>SEU CNPJ/ SUA EMPRESA</p>
        <strong>
          <p>
            [{this.state.province}] [{this.state.cnpj}] {this.state.companyName}
          </p>
        </strong>
      </div>
    );
  }
}

export default CompanyInformation;
