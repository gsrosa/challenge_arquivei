import React from 'react';
import { shallow } from 'enzyme';
import CompanyInformation from './index';

import EnzymeConfig from '../../enzyme-config';

describe('CompanyInformation Component', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<CompanyInformation />);
    expect(wrapper).toMatchSnapshot();
  });
});
