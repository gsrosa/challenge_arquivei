import React from 'react';
import { shallow } from 'enzyme';
import SubHeader from './index';

import EnzymeConfig from '../../enzyme-config';

describe('SubHeader Component', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<SubHeader />);
    expect(wrapper).toMatchSnapshot();
  });
});
