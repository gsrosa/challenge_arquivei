import React from 'react';

const SubHeader = () => {
  return (
    <div className="sub-header">
      <h1>Existem novas notas contra seu CNPJ</h1>
    </div>
  );
};

export default SubHeader;
