import React from 'react';

const Factory = ({ handleCloseModal }) => (
  <div className="factory-gif">
    <span className="btn-close" onClick={handleCloseModal} />
    {process.env.NODE_ENV !== 'test' ? (
      <img src={require('../../assets/img/factory.gif')} />
    ) : (
      ''
    )}
    <p>Descarregando para a nuvem</p>
  </div>
);

export default Factory;
