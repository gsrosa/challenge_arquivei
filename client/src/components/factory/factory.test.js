import React from 'react';
import { shallow } from 'enzyme';
import Factory from './index';

import EnzymeConfig from '../../enzyme-config';

describe('Factory Component', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<Factory />);
    expect(wrapper).toMatchSnapshot();
  });
});
