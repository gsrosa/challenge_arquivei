import React from 'react';
import '../../assets/stylesheets/style.scss';

const Header = () => {
  return (
    <div className="main-header">
      {process.env.NODE_ENV !== 'test' ? (
        <img
          src={require('../../assets/img/logo_branco.png')}
          alt="company_logo"
        />
      ) : (
        ''
      )}
    </div>
  );
};

export default Header;
