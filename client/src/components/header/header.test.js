import React from 'react';
import { shallow } from 'enzyme';
import Header from './index';

import EnzymeConfig from '../../enzyme-config';

describe('Header Component', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper).toMatchSnapshot();
  });
});
