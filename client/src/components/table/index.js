import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faDownload, faEye } from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import { format as formatCnpj } from '@fnando/cnpj';
import moment from 'moment';

import { uri } from '../company-information';

library.add(faDownload, faEye);

class Table extends Component {
  constructor() {
    super();
    this.state = {
      status: '',
      number: '',
      emission: '',
      supplier: '',
      value: 0,
      cnpj: ''
    };
  }

  componentWillMount() {
    axios.get(`${uri}/initial-state`).then(res => {
      this.setState({
        status: res.data.networkGrowth.data[0].status,
        number: res.data.networkGrowth.data[0].number,
        emission: res.data.networkGrowth.data[0].emissionDate,
        supplier: res.data.networkGrowth.data[0].emitter.XNome,
        value: res.data.networkGrowth.data[0].value,
        cnpj: formatCnpj(res.data.networkGrowth.data[0].emitter.CNPJ)
      });
    });
  }

  render() {
    return (
      <div className="content-table">
        <table align="center">
          <thead>
            <tr>
              <th className="table-title">Status</th>
              <th className="table-title">Número</th>
              <th className="table-title">Emissão</th>
              <th className="table-title">Fornecedor</th>
              <th className="table-title">Valor</th>
              <th className="table-title">CNPJ</th>
              <th className="table-title">Ação</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="table-content">
                <div className="authorized">
                  {this.state.status
                    .replace(this.state.status, 'Autorizada')
                    .toUpperCase()}
                </div>
              </td>
              <td className="table-content">{this.state.number}</td>
              <td className="table-content">
                {moment(this.state.emission).format('DD/MM/Y')}
              </td>
              <td className="table-content">{this.state.supplier}</td>
              <td className="table-content">
                R$
                {this.state.value}
                ,00
              </td>
              <td className="table-content">{this.state.cnpj}</td>
              <td className="table-content">
                <button className="btn-action">
                  <FontAwesomeIcon icon="eye" />
                  &nbsp; Ver Nota
                </button>
                <button className="btn-action">
                  <FontAwesomeIcon icon="download" />
                  &nbsp; Baixar XML
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
