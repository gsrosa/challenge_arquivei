import React from 'react';
import { shallow } from 'enzyme';
import Table from './index';

import EnzymeConfig from '../../enzyme-config';

describe('Table Component', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<Table />);
    expect(wrapper).toMatchSnapshot();
  });
});
