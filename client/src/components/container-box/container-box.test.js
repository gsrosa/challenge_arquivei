import React from 'react';
import { shallow } from 'enzyme';
import ContainerBox from './index';

import EnzymeConfig from '../../enzyme-config';

describe('ContainerBox Component with Modal', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<ContainerBox />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should update state when setState is called', () => {
    const wrapper = shallow(<ContainerBox />);

    wrapper.setState({ modalIsOpen: true });

    expect(wrapper.state('modalIsOpen')).toEqual(true);
  });

  it('should update ContainerBox state when setState is called', () => {
    const wrapper = shallow(<ContainerBox />);

    wrapper.setState({ modalFactoryIsOpen: true });

    expect(wrapper.state('modalFactoryIsOpen')).toEqual(true);
  });
});
