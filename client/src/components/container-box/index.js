import React, { Component } from 'react';
import Modal from 'react-modal';
import ProgressBar from '../progress-bar';
import ModalContent from '../modal-content';
import Factory from '../factory';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

if (process.env.NODE_ENV !== 'test') Modal.setAppElement('#root');

class ContainerBox extends Component {
  constructor() {
    super();
    this.state = {
      modalIsOpen: false,
      modalFactoryIsOpen: false
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.openModalFactory = this.openModalFactory.bind(this);
    this.closeModalFactory = this.closeModalFactory.bind(this);
  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  openModalFactory() {
    this.setState({ modalFactoryIsOpen: true });
    setTimeout(() => {
      this.closeModal();
    }, 100);
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  closeModalFactory() {
    this.setState({ modalFactoryIsOpen: false });
  }

  render() {
    return (
      <div className="container-box">
        Você pode ter as notas de <strong>todos os seus fornecedores</strong>,
        que ter acesso a ela?
        <strong> Experimente grátis o Arquivei</strong> e tenha todas
        diretamente da <strong>Sefaz</strong>
        <button className="btn-try-arquivei" onClick={this.openModal}>
          Experimentar o Arquivei
        </button>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Experimente grátis o Arquivei"
        >
          <ModalContent
            handleModalFactory={this.openModalFactory}
            handleCloseModal={this.closeModal}
          />
        </Modal>
        <Modal
          isOpen={this.state.modalFactoryIsOpen}
          style={customStyles}
          onRequestClose={this.closeModalFactory}
        >
          <Factory handleCloseModal={this.closeModalFactory} />
          <ProgressBar />
        </Modal>
      </div>
    );
  }
}

export default ContainerBox;
