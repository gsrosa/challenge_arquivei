import React from 'react';
import { shallow } from 'enzyme';
import ModalContent from './index';

import EnzymeConfig from '../../enzyme-config';

describe('ModalContent Component', () => {
  it('should render without crash', () => {
    const wrapper = shallow(<ModalContent />);
    expect(wrapper).toMatchSnapshot();
  });
});
