import React from 'react';

const ModalContent = ({ handleModalFactory, handleCloseModal }) => (
  <div className="modal">
    <h3>
      Experimente grátis o Arquivei 🎉{' '}
      <span className="btn-close" onClick={handleCloseModal}>
        X
      </span>
    </h3>
    <p>
      Com o <strong>Arquivei</strong>, você terá acesso a{' '}
      <strong>todas as notas</strong> dos seus fornecedores, além de:
    </p>
    <ul>
      <li>
        <strong>Consulta</strong> de seus <strong>XMLs</strong> direto do{' '}
        <strong>SEFAZ</strong>;
      </li>
      <li>
        <strong>Alerta</strong> de notas canceladas;
      </li>
      <li>
        <strong>Conhecimento</strong> de notas <strong>indevidas/frias</strong>;
      </li>
    </ul>
    <p>
      <strong>Tudo</strong> isso <strong>grátis</strong> e sem compromisso.
    </p>
    <button className="btn-try-now" onClick={handleModalFactory}>
      Experimentar Agora
    </button>
    <p className="p-terms">
      Ao continuar você aceita o{' '}
      <span className="terms">Termo de uso do Arquivei</span>
    </p>
  </div>
);

export default ModalContent;
